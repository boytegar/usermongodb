var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    uom: {
        type: Number,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Cagetory',
        required: true
    },
    image: {
        type: String,
        required: true
    }

});

// Getter
productSchema.path('price').get( (num) => {
    return (num / 100).toFixed(2);
});

// Setter
productSchema.path('price').set( (num) =>{
    return num * 100;
});

var Product = module.exports = mongoose.model('Product', productSchema);

module.exports.get = function (callback, limit) {
    Product.find(callback).limit(limit);
}