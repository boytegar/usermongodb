var mongoose = require('mongoose');
let Schema = mongoose.Schema;
// Setup schema
var userSchema = Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    gender: String,
    phone: {
        type: String,
        required: true
    }, 
    create_date: {
        type: Date,
        default: Date.now
    },
    type_user: {
        type: String,
        default: 'trial'
    },
    password: {
        type: String,
        default: '12345678'
    }
    
});

var User = module.exports = mongoose.model('User', userSchema);

module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}