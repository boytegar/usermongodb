var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var categorySchema = Schema({
    name: {
        type: String,
        required: true
    }
});

var Category = module.exports = mongoose.model('Category', categorySchema);

module.exports.get = function (callback, limit) {
    Category.find(callback).limit(limit);
}