var mongoose = require('mongoose');
let Schema = mongoose.Schema;

var marketSchema = Schema({
    name : {
        type: String,
        required: true
    },
    latitude: {
        type: String,
        required: true
    },
    longitude: {
        type: String,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    type: {
        type: String,
        require: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

var Market = module.exports = mongoose.model('Market', marketSchema);

module.exports.get = function (callback, limit) {
    Market.find(callback).limit(limit);
}