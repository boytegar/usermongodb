
const User = require('../model/User');

exports.index = (req, res) => {
    User.find()
        .then(users => {
            res.json({
                status: "success",
                message: "Users Successfully",
                data: users
            });
        }).catch(error => {
            res.send(error);
        })
};

exports.new = (req, res) => {
    var users = User(req.body);
    users.save((err) => {
        if (err)
            res.json({
                status: 'error',
                message: err.message,
                data: []
            });
        res.json({
            status: 'ok',
            message: "New User Created!",
            data: users
        });
    });
};

// Handle view actions
exports.findId = (req, res) => {
    User.find({ _id: req.params._id })
        .populate('market')
        .then(users => {

            res.send(users)


        }).catch(err => {
            res.send(err)
        })
};

exports.login = (req, res) => {
    User.findOne({ phone: req.query.phone, password: req.query.password }, (err, users) => {
        if (users) {
            res.json({
                status: 'ok',
                message: 'Users Ditemukan',
                data: users
            });
        } else {
            res.json({
                status: 'error',
                message: 'not find data',
                data: {}
            });
        }
    })

}

// Handle update actions
exports.update = (req, res) => {
    User.findById(req.params._id, (err, users) => {
        if (err)
            res.send(err)
        users = User(req.body)

        // Save and validate
        users.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: "User Updated!",
                data: users
            });
        });
    });
};

// Handle delete actions
exports.delete = function (req, res) {
    User.deleteOne({
        _id: req.params._id
    }, (err, users) => {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: "User Deleted!"
        });
    });
};