const faker = require('faker');
const data = require('../model/product');

exports.index = (req, res) => {

    let products = [];

  for (let id=1; id <= 10; id++) {
      
    product = data();
    product.name = faker.commerce.productName();
    product.price = faker.commerce.price();
    product.description = faker.lorem.paragraph();
    product.uom = faker.random.number({'max': 50});
    product.image = faker.image.food();
    product.category = '5f27a9e48aa5184d988c0a9f';

    products.push(product);
  }

   res.json({
     status: 'ok',
     message: 'get data success',
     data: products
   });
}