
const Market = require('../model/Market');
//const User = require('../model/user');

exports.index =  (req, res) => {
     Market.find( (err, market) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Users Successfully",
            data: market
        });
    });
};

exports.new =  (req, res)  =>  {
    var markets = new Market(req.body);
  //  let user =  User.findOne({_id: req.body._id});
  //  markets.user = user._id;
    markets.save( (err) => {
        if (err)
            res.json(err);
        res.json({
            message: "New Market Created!",
            data: markets
        });
    });
};

exports.getById = (req, res) => {
    Market.findById(req.params._id, function (err, market) {
        if (err)
            res.send(err);
        res.json({
            message: "Market Details Loading...",
            data: market
        });
    });
};

//get list market
exports.view = (req, res) => {
    Market.find(req.params.user_id, (err, market) => {
        if(err) res.send(err);
        res.json({
            status: 'ok',
            message: "Sukses Mengambil Data",
            data: market
        })
    })
};

exports.update =  (req, res) => {
    Mar.findById(req.params._id,  (err, market) => {
        if (err)
            res.send(err)
            market = Market(req.body)

        // Save and validate
        market.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: "Market Updated!",
                data: market
            });
        });
    });
};

// Handle delete actions
exports.delete = function (req, res) {
    Market.deleteOne({
        _id: req.params._id
    }, function (err, market) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: "User Deleted!"
        });
    });
};