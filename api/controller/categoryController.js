const Category = require('../model/Category');

exports.index = (req, res) => {
    Category.find().then(category => {
        res.json({
            status: "success",
            message: "Data Category Successful",
            data: category
        });
    }).catch(error => {
        res.send(error);
    });
}

exports.insert = (req, res) =>{
    var category = Category(req.body);
    category.save().then(category => {
         res.json({
             status: 'ok',
             message: 'Insert Data Category Success',
             data: {}
         });
    }).catch(err =>{
        res.send(err);
    })
}