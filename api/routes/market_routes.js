var express  = require('express');
var router = express.Router();
var app = router.route;

var market = require('../controller/marketController')

    router.route('/market')
        .get(market.index)
        .post(market.new);
        router.route('/market/:_id')
        .get(market.view)
        .patch(market.update)
        .put(market.update)
        .delete(market.delete);
        router.route('/market/:user_id')
        .get(market.view);

module.exports = router;

    
