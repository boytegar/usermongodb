var express = require('express');
var router = express.Router();

var product = require('../controller/productController.js');

router.route('/product')
    .get(product.index);

module.exports = router;