var express = require('express');
var router = express.Router();

var user = require('../controller/userController.js');

router.route('/user')
    .get(user.index).post(user.login)
    .post(user.new);

router.route('/user/:_id')
    .get(user.findId)
    .patch(user.update)
    .put(user.update)
    .delete(user.delete);

module.exports = router;