var express = require('express');
var router = express.Router();
var app = router.route;

var category = require('../controller/categoryController')

router.route('/category')
.get(category.index)
.post(category.insert);

module.exports = router;