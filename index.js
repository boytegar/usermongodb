const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

// middlewares
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/db', {
    useNewUrlParser: true,
    useUnifiedTopology: true 
});
//app.use(express.json());
// Use API Routes
var api = '/api';

let marketRoutes = require("./api/routes/market_routes");
app.use(api, marketRoutes);
let userRoutes = require('./api/routes/user_routes');
app.use(api, userRoutes)
let categoryRoutes = require('./api/routes/category_routes');
app.use(api, categoryRoutes);
let productRoutes = require('./api/routes/product_routes');
app.use(api, productRoutes);



var db = mongoose.connection;

// Setup server port
var port = process.env.port || 8888;

// Send response
app.get('/', (req, res) => res.send('Hello From Express'));

// Launch app
app.listen(port, function () {
    console.log("Running Rest API in port " + port);
});

//for debugging mobile local
// app.listen(port, '0.0.0.0', function(){
// console.log("Running Rest API in port " + port);
// });